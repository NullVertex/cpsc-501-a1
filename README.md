# CPSC 501 Assignment 1
Cole Towstego (10089693)

## Refactor 1
Used the `Extract Superclass` refactoring to extract the abstract class `Employee` from `Cashier` and `Supervisor` classes, allowing for further types of Employees to be added in the future and reducing duplicate code.

## Refactor 2
Used the `Rename Method` refactoring to rename `Employee.name()` to `Employee.getName()` improving readability.

## Refactor 3
Used the `Rename Method` refactoring to rename `Employee.hired()` to `Employee.dateHired()` improving readability.

## Refactor 4
Used the `Rename Method` refactoring to rename `Employee.pay()` to `Employee.calculatePay()` improving readability.

## Refactor 5
Used the `Replace Magic Number with Symbolic Constant` refactoring to assign a `BASE_HOURLY_WAGE` constant to all employees and a `SUPERVISOR_WAGE_DIFFERENTIAL` constant for supervisors, explaining intent in the `calculatePay()` methods.

## Refactor 6
Used the `Rename Method` refactoring to rename `Employee.num()` to `Employee.getNumber()` improving readability.