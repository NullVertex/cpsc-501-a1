.PHONY: default all run build tests docs clean

J=java
JC=javac
JD=javadoc
WD=$(shell pwd)
CLASSPATH="$(WD)/junit/junit.jar:$(WD)/junit/hamcrest-core.jar:$(WD)/bin:$(WD)/src:$(WD)/tests"
JFLAGS=-cp $(CLASSPATH)
JCFLAGS=-cp $(CLASSPATH) -d "$(WD)/bin"
JDFLAGS=-d ./docs

.SUFFIXES: .java .class

default: build

all: clean build tests docs

run:
	@$(J) $(JFLAGS) Main

build:
	@$(JC) $(JCFLAGS) $(wildcard ./src/*.java)
	@echo "Generated Java classes:"
	@ls ./bin/*.class | cat

tests:
	@$(JC) $(JCFLAGS) $(wildcard ./tests/*.java)
	@echo $(TESTS)
	@$(J) $(JFLAGS) org.junit.runner.JUnitCore TestCashier TestSupervisor

docs:
	@echo "Generating Documentation in ./docs/"
	@$(JD) $(JDFLAGS) $(wildcard ./src/*.java)

clean:
	@echo "Cleaning all Java class files..."
	@$(RM) ./bin/*.class