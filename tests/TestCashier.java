import org.junit.Test;
import static org.junit.Assert.*;

public class TestCashier
{
    @Test
    public void testCreate()
    {
        new Cashier(100, "First", "Last");
        new Cashier(101, "Abe", "Lincoln", "2016/10/31");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badCreate()
    {
        new Cashier(26, "Donald", "Trump", "@asvndosaf");
    }

    @Test
    public void testPayCalculation()
    {
        Cashier c = new Cashier(999, "First", "Last");
        assertEquals(60.00, c.calculatePay(4), 0.01);
    }

    @Test
    public void testName()
    {
        assertTrue("Bob Doe".equals(new Cashier(1, "Bob", "Doe").getName()));
    }
}