import org.junit.Test;
import static org.junit.Assert.*;

public class TestSupervisor
{
    @Test
    public void testCreate()
    {
        new Supervisor(100, "First", "Last");
        new Supervisor(101, "Abe", "Lincoln", "2016/10/31");
    }

    @Test(expected = IllegalArgumentException.class)
    public void badCreate()
    {
        new Supervisor(26, "Donald", "Trump", "@asvndosaf");
    }

    @Test
    public void testPayCalculation()
    {
        Supervisor c = new Supervisor(999, "First", "Last");
        assertEquals(80.00, c.calculatePay(4), 0.01);
    }

    @Test
    public void testName()
    {
        assertTrue("Bob Doe".equals(new Supervisor(1, "Bob", "Doe").getName()));
    }
}