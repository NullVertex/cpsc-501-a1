public class Cashier extends Employee
{
    public Cashier(int num, String fName, String lName)
    {
        super(num, fName, lName);
    }

    public Cashier(int num, String fName, String lName, String hireDate) throws IllegalArgumentException
    {
        super(num, fName, lName, hireDate);
    }

    public double calculatePay(double hours_worked)
    {
        return hours_worked * BASE_HOURLY_WAGE;
    }
}