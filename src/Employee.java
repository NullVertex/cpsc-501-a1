import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class Employee
{
    public static final double BASE_HOURLY_WAGE = 15.00;

    private int number;
    private String firstName;
    private String lastName;
    private Date hired;

    public Employee(int num, String fName, String lName)
    {
        number = num;
        firstName = fName;
        lastName = lName;
        hired = new Date();
    }

    public Employee(int num, String fName, String lName, String hireDate) throws IllegalArgumentException
    {
        try
        {
            number = num;
            firstName = fName;
            lastName = lName;

            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            hired = dateFormat.parse(hireDate);
        }
        catch (ParseException e)
        {
            throw new IllegalArgumentException("Incorrect date format \"YYYY/MM/dd\"");
        }
    }

    abstract double calculatePay(double hours_worked);

    public int getNumber()
    {
        return number;
    }

    public String getName()
    {
        return firstName + " " + lastName;
    }

    public Date dateHired()
    {
        return hired;
    }
}