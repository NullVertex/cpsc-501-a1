import java.lang.*;
import java.util.Vector;

public class Main
{
    public static void main(String[] args)
    {
        Vector<Cashier> cashiers = new Vector<Cashier>();
        
        try {
            cashiers.add(new Cashier(10, "Bob", "Doe", "2000/06/01"));
            cashiers.add(new Cashier(11, "Jane", "Doe", "2001/01/01"));
            cashiers.add(new Cashier(12, "Steve", "Smith", "2010/10/31"));
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid date entered");
        }

        System.out.println("Cashiers: ");
        for(int c=0; c < cashiers.size(); c++)
        {
            System.out.println(cashiers.get(c).getNumber() + ": " + cashiers.get(c).getName());
        }

        Vector<Supervisor> supers = new Vector<Supervisor>();
        
        try {
            supers.add(new Supervisor(1, "Sean", "Smith", "2000/06/01"));
            supers.add(new Supervisor(2, "Kyle", "Smith", "2001/01/01"));
            supers.add(new Supervisor(3, "Glenn", "Doe", "2010/10/31"));
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid date entered");
        }

        System.out.println("Supervisors: ");
        for(int c=0; c < supers.size(); c++)
        {
            System.out.println(supers.get(c).getNumber() + ": " + supers.get(c).getName());
        }
    }
}