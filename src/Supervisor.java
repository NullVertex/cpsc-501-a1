public class Supervisor extends Employee
{
    public static final double SUPERVISOR_WAGE_DIFFERENTIAL = 5.00;

    public Supervisor(int num, String fName, String lName)
    {
        super(num, fName, lName);
    }

    public Supervisor(int num, String fName, String lName, String hireDate) throws IllegalArgumentException
    {
        super(num, fName, lName, hireDate);
    }

    public double calculatePay(double hours_worked)
    {
        return hours_worked * (BASE_HOURLY_WAGE + SUPERVISOR_WAGE_DIFFERENTIAL);
    }
}